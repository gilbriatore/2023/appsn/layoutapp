package br.edu.up.applayout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.commit
import androidx.fragment.app.replace

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun carregarLinear(view: View) {
        supportFragmentManager.commit{
            replace<LinearFragment>(R.id.fragmentContainerView)
        }

    }
    fun carregarFrame(view: View) {
        supportFragmentManager.commit{
            replace<FrameFragment>(R.id.fragmentContainerView)
        }
    }
    fun carregarConstraint(view: View) {
        supportFragmentManager.commit {
            replace<ConstraintFragment>(R.id.fragmentContainerView)
        }
    }
}